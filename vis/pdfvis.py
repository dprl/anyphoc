import subprocess
from typing import Tuple
import tempfile
from bs4 import BeautifulSoup
from os import system
from PyPDF2 import PdfReader


def pdf_to_svg(file_path: str, page: int) -> str:
    tmp_file = tempfile.NamedTemporaryFile(delete=False)
    tmp_file.close()
    file_path = file_path.rstrip('\n')
    reader = PdfReader(file_path)
    try:
        page = page + reader.trailer["/Root"]["/PageLabels"]["/Nums"][1]['/St']
    except Exception as e:
        page = page + 1


    print(page)
    system(f"pdf2svg {file_path} {tmp_file.name} {page}")
    with open(tmp_file.name, "r") as fp:
        return fp.read()


def crop_svg(svg_str: str, crop_box: Tuple[float, float, float, float]) -> str:
    soup = BeautifulSoup(svg_str, "lxml")
    svg_soup = soup.find("svg")
    svg_soup["viewBox"] = f"{crop_box[0]} {crop_box[1]} {crop_box[2]} {crop_box[3]}"
    svg_soup["viewbox"] = f"{crop_box[0]} {crop_box[1]} {crop_box[2]} {crop_box[3]}"
    svg_soup["width"] = f"{crop_box[2]}pt"
    svg_soup["height"] = f"{crop_box[3]}pt"
    return str(svg_soup)

