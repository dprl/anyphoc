

all:
	./bin/install

clean-repos:
	rm -rf lgeval
	rm -rf protables

conda-remove:
	conda env remove -n anyphoc

rebuild: conda-remove clean-repos all

speed-benchmarks:
	./bin/speed-benchmarks

pro-playground:
	./bin/playground/pro
