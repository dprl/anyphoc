from typing import Dict, List
import ctypes
import numpy


def compress_bitvectors(list_phocs: Dict[str, List], index_type=ctypes.c_int64) -> Dict[str, int]:
    int_phocs = {}
    unified_symbols = list_phocs.pop('__unifiedkeys__', [])
    for k in list_phocs.keys():
        if index_type == "hex":
            int_phocs[k] = compress_bitvector_hex(list_phocs[k])
        elif index_type == "symbol-percentage":
            int_phocs[k] = list_phocs[k]
        elif type(index_type) == str:
            int_phocs[k] = compress_bitvector(list_phocs[k], ctypes.c_int64)
        else:
            int_phocs[k] = compress_bitvector(list_phocs[k], index_type)

    list_phocs['__unifiedkeys__'] = unified_symbols
    return int_phocs


def compress_bitvector(phoc: List[int], index_type):
    bit_string = int(''.join(str(i) for i in phoc), 2)
    return index_type(bit_string).value


def compress_bitvector_hex(phoc: List[int]):
    remaining_zeros = len(phoc) % 4
    for _ in range(4 - remaining_zeros):
        phoc.insert(0, 0)
    chunks_4 = numpy.array(phoc).reshape((int(len(phoc) / 4), 4))
    hex_string = ''
    for i in range(len(chunks_4)):
        chunk = chunks_4[-i - 1]
        hex_string = get_char(''.join(str(j) for j in chunk)) + hex_string
    return hex_string


def get_char(binary):
    number = int(binary, 2)
    letters = ['A', 'B', 'C', 'D', 'E', 'F']
    if number < 10:
        return str(number)
    else:
        return letters[number - 10]
