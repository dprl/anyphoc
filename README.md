# AnyPhoc

*authors*: Matt Langsenkamp

Based on previous work by Robin Avenoso

(Document and Pattern Recognition Lab (dprl), Rochester Institute of Technology, USA)

## Table of Contents

- [Installation](#installation)
- [Pattern](#pattern)
- [Support](#support)

## Installation

This code was developed and tested on Ubuntu 20.04
Make sure you have python and the Conda package manager installed. If not it can be downloaded from here: [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)
Make sure you have Docker installed. If not it can be downloaded from here: [get docker](https://docs.docker.com/get-docker/)

Run the following commands to set up a conda environment

```
$ conda create -n anyphoc python=3.10
$ conda activate anyphoc
(anyphoc) $ pip install -r requirements.txt
```

If not installed already install libpangocairo `sudo apt-get install libpangocairo-1.0-0`

To test if installation was successful run `pytest`. A couple of passing tests should be printed to the console.

next run `docker run -p 8081:8081 dprl/mathjax-render` a docker container will download if it doesn't exist locally, and run. Eventually a server will come alive stating it is listening on port 8081. `Ctrl-c` to stop the server.

finally run `python playground/playground.py` to see an example of how the library may be used.
## Pattern

The idea behind the anyphoc library is that you can extract pyramidal histograms of characters (or tokens) from any render-able data structure. 

There are a couple main concepts:

- **source**: where that data comes from, so far just svgs are supported.
- **canvas**: a set of candidates that contain information about their relative position in the source, and canvas metadata that describes attributes about the source. 
- **phoc definitions**: objects that describe the phocs that will be extracted from a canvas. Multiple phocs can be composed together and tested against a canvas.

## Support
This material is based upon work supported by the National Science Foundation
(USA) under Grant Nos. IIS-1717997 (MathSeer), and 2019897 (MMLI), and the
Alfred P. Sloan Foundation under Grant No. G-2017-9827.
Any opinions, findings and conclusions or recommendations expressed in this
material are those of the author(s) and do not necessarily reflect the views of
the National Science Foundation or the Alfred P. Sloan Foundation.
