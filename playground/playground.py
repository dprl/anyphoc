import json
import os.path
import time
import bs4
from canvas.converters.svgcanvas import render, svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.phoccompose import PhocCompose
from phoc.vertline.vertlinephoc import VertLinePhoc
from phoc.rectangle.rectanglephoc import RectanglePhoc

from indexutils.compressbitvectors import compress_bitvectors

if __name__ == '__main__':
    problematic_latex = [
        r'\frac{1 + \sqrt{5}}{2}']

    for i, lat in enumerate(problematic_latex):
        svg_data = render(lat,
                          "http://127.0.0.1:8045/render/")

        # with open('tests/data/test.svg', 'w') as f:
        #    f.write(bs4.BeautifulSoup(svg_data, 'lxml').prettify())
        start = time.time()
        canvas_ = svg_to_svg_canvas(svg_data, region_type="line")
        end = time.time()
        print("time: ", end - start)
        vert_phoc = VertLinePhoc(0, 10, starting_level=1, skipping_step=2)
        horz_phoc = HorzLinePhoc(0, 2, percentages=True)
        ell_phoc = EllipsisPhoc(5,percentages=True)
        rec_phoc = RectanglePhoc(5,percentages=True)

        compose = PhocCompose([
            rec_phoc
        ], skip_first_level=True)
        print(compose.explain())

        start = time.time()
        tested = compose.test(canvas_)
        end = time.time()
        print("time: ", end - start)

        print(compress_bitvectors(tested, index_type='symbol-percentage'))
        compose.build_html(canvas_, os.path.join("html", f'odd_even_test'), "ok")

