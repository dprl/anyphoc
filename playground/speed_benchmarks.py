import time
import pandas as pd
import os
from canvas.converters.svgcanvas import render, svg_to_svg_canvas
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.phoccompose import PhocCompose
from phoc.vertline.vertlinephoc import VertLinePhoc

if __name__ == '__main__':

    df = pd.read_csv(os.path.join('inputs', 'latexbigs.tsv'), sep="\t")

    sum = 0
    sum_all = 0
    print('big formulas: ')
    for index, row in df.iterrows():
        svg = render(row.formula, "http://127.0.0.1:8081/render/")
        start_all = time.time()
        canvas_ = svg_to_svg_canvas(svg)
        vert_phoc = VertLinePhoc(0, 4)
        horz_phoc = HorzLinePhoc(0, 4)
        compose = PhocCompose([
            vert_phoc,
            horz_phoc
        ])
        start = time.time()
        tested = compose.test(canvas_)
        end = time.time()
        t = end - start
        sum += t
        sum_all += end - start_all
    print('average test time taken: ', sum/len(df))
    print('average test + canvas time taken: ', sum_all/len(df))

    print("")
    print('little formulas:')
    df = pd.read_csv(os.path.join('inputs', 'Formula_topics_latex_V2.0_100.tsv'), sep="\t")
    sum = 0
    sum_all = 0
    for index, row in df.iterrows():
        svg = render(row.formula, "http://127.0.0.1:8081/render/")
        start_all = time.time()
        canvas_ = svg_to_svg_canvas(svg)
        vert_phoc = VertLinePhoc(0, 4)
        horz_phoc = HorzLinePhoc(0, 4)
        compose = PhocCompose([
            vert_phoc,
            horz_phoc
        ])
        start = time.time()
        tested = compose.test(canvas_)
        end = time.time()
        t = end - start
        sum += t
        sum_all += end - start_all
    print('average test time taken: ', sum / len(df))
    print('average test + canvas time taken: ', sum_all / len(df))

