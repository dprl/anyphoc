import json
import os.path
import os
import time
from typing import List
from phoc.horzline.horzlinephoc import HorzLinePhoc
from protables.ProTables import read_pro_tsv
from canvas.converters.procanvas import protable_to_region_canvas_list
from phoc.phoccompose import PhocCompose
from phoc.vertline.vertlinephoc import VertLinePhoc

if __name__ == '__main__':

    vert_phoc = VertLinePhoc(0, 1)
    horz_phoc = HorzLinePhoc(0, 4)
    compose = PhocCompose([
            vert_phoc,
            #horz_phoc,
        ])
    pro = read_pro_tsv('inputs/ACL/tsv/peng-etal-2015-joint-out.tsv')
    documents = pro[0]
    page_widths = pro[1]
    l_of_l_of_canvas = protable_to_region_canvas_list(pro)

    for canvas in l_of_l_of_canvas[0]:
        compose.test(canvas)

    canvas = l_of_l_of_canvas[0][0]
    compose.build_html(canvas, f'lets_11_see', "ok")



