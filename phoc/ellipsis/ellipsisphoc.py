from typing import List, Tuple, Dict, Hashable, Callable

import numpy as np
import shapely
import svgutils
from shapely.geometry import Point
from shapely.affinity import scale, rotate

from canvas import Candidate
from canvas.canvasmeta import Geo2DCanvasMeta
from canvas.pro.regioncanvas import RegionCanvas
from canvas.svg.svgcandidate import SVGCandidate
from canvas.svg.svgcanvas import SVGCanvas
from phoc.ellipsis.ellipsisphocsplit import EllipsisPhocSplit
from phoc.phoc import Phoc, pull_svg_contents
from phoc.phocsplit import PhocSplit
from vis.pdfvis import crop_svg, pdf_to_svg


class EllipsisPhoc(Phoc):

    def __init__(self, degree_n: int, rotate_degree: float = 0, glue_to_height: bool = True,
                 glue_to_width: bool = True, glue_to_larger: bool = False, scale_factor: float = None, name: str = None,
                 level_scale: Callable[[int], float] = lambda x: 1, skipping_step: int = 1, starting_level: int=1, percentages: bool = False):

        super().__init__(degree_n, level_scale, skipping_step, starting_level=starting_level, percentages=percentages)
        self.degree_n = degree_n
        self.rotate_degree = rotate_degree
        self.glue_to_height = glue_to_height
        self.glue_to_width = glue_to_width
        self.glue_to_larger = glue_to_larger
        self.scale_factor = scale_factor
        self.name = name        
        if name is None:
            self.name = f"ellipsis_level_{degree_n}"

    def test(self, canvas: SVGCanvas) -> Dict[Hashable, List]:

        symbol_dict = {"__unifiedkeys__": []}
        _, tot_vect_len = self.explain()
        split_count = 0
        levels = self.expand_splits(canvas.canvas_meta)
        for deg in levels:
            for split in deg:
                hits = canvas.candidates.intersection(split.bounds, objects='raw')
                for hit in hits:
                    candidate: SVGCandidate = hit
                    if candidate.unified:
                        if candidate.key not in symbol_dict["__unifiedkeys__"]:
                            symbol_dict["__unifiedkeys__"].append(candidate.key)
                    bit_val = split.test(candidate)
                    if candidate.key in symbol_dict.keys():
                        cur_vector = symbol_dict[candidate.key]
                        cur_vector[split_count] = max(bit_val, cur_vector[split_count])
                    else:
                        cur_vector = [0] * tot_vect_len
                        cur_vector[split_count] = bit_val
                        symbol_dict[candidate.key] = cur_vector
                split_count += 1

        return symbol_dict

    def test_candidate(self, candidate, canvas_meta) -> List:

        result = []
        splits: List[List[PhocSplit]] = self.expand_splits(canvas_meta)
        for deg in splits:
            for split in deg:
                result.append(split.test(candidate))
        return result

    def explain(self, degree_n=None) -> Tuple[str, int]:
        split_sum = 0
        for deg in range(self.starting_level, self.n + 1, self.skipping_step):
            # ie degree 1 has 2 regions
            splits_per_degree = deg + 1
            for split_n in range(splits_per_degree):
                split_sum += 1
        return "", int(split_sum)

    def visualize(self, canvas) -> List[str]:

        split_svgs = []
        for i, deg in enumerate(self.expand_splits(canvas.canvas_meta, vis=True)):
            bbox = canvas.canvas_meta.bounding_box.polygon.bounds
            bounds = (bbox[0], bbox[1], (bbox[2] - bbox[0]), (bbox[3] - bbox[1]))
            if isinstance(canvas, RegionCanvas):
                cur_svg = pdf_to_svg(canvas.file_name, canvas.page_number)
                cur_svg_full = svgutils.transform.fromstring(cur_svg)
                s_factor = 256 / 72

                figure = svgutils.compose.Figure(
                    float(cur_svg_full.height.strip("pt")) * s_factor,
                    float(cur_svg_full.width.strip("pt")) * s_factor,
                    cur_svg_full.getroot().scale(s_factor, s_factor))
                cur_svg = figure.tostr()
                cur_svg = crop_svg(cur_svg, bounds)
                multi = .1
            else:
                cur_svg = canvas.svg
                multi = 1
            for s in deg:
                cur_svg = pull_svg_contents(cur_svg, s.visualize(multi), bounds)

            for i in canvas.candidates.intersection((-1000000, -10000000, 1000000, 1000000), objects=True):
                cand: SVGCandidate = i.object
                minx, miny, maxx, maxy = cand.candidate_region.bounds
                x = minx
                y = miny
                height = maxy - miny
                width = maxx - minx
                height = max(height, 1)
                width = max(width, 1)
                if height == 1 or width == 1:
                    stroke_width = 30*multi*multi
                else:
                    stroke_width = 5*multi
                r = f'<rect x="{x}" y="{y}" width="{width}" height="{height}" style="fill:blue;stroke:red;stroke-width:{stroke_width};fill-opacity:0.05;stroke-opacity:1" />'
                cur_svg = pull_svg_contents(cur_svg, r, None)
            split_svgs.append(cur_svg)
        return split_svgs

    def expand_splits(self, canvas_meta, vis=False) -> List[List[PhocSplit]]:

        split_list = []
        for deg in range(self.starting_level, self.degree_n + 1, self.skipping_step):
            splits = []
            # ie degree 1 has 2 regions
            splits_per_degree = deg+1
            for split_n in range(splits_per_degree):
                splits.append(
                    EllipsisPhocSplit(
                        canvas_meta,
                        degree_n=deg,
                        split_n=split_n,
                        rotate_degree=self.rotate_degree,
                        glue_to_height=self.glue_to_height,
                        glue_to_width=self.glue_to_width,
                        glue_to_larger=self.glue_to_larger,
                        scale_factor=self.scale_factor,
                        vis=vis,
                        percentages=self.percentages
                    ))
            split_list.append(splits)

        return split_list
