from copy import copy
from shapely.prepared import prep
import bs4
from shapely.geometry import Point
from shapely.affinity import scale, rotate
from canvas import Geo2DCandidate
from canvas.boundingbox2d import BoundingBox2D
from canvas.canvasmeta import Geo2DCanvasMeta
from phoc.phocsplit import PhocSplit, get_color


class EllipsisPhocSplit(PhocSplit):

    def __init__(
            self,
            canvas_meta: Geo2DCanvasMeta,
            degree_n: int,
            split_n: int,
            rotate_degree: float = 0,
            glue_to_height: bool = True,
            glue_to_width: bool = True,
            glue_to_larger: bool = False,
            scale_factor: float = None,
            vis: bool = False,
            percentages=False):

        self.percentages = percentages

        self.split_n = split_n
        # must be glued to either height or width, as ellipsis need a frame of reference
        assert glue_to_width or glue_to_height or glue_to_larger
        # can't specify what dimension to glue to if you will just glue to larger
        assert (glue_to_larger and (not glue_to_width) and (not glue_to_height)) \
               or ((glue_to_width or glue_to_height) and not glue_to_larger)
        if glue_to_larger:
            if canvas_meta.bounding_box.width > canvas_meta.bounding_box.height:
                glue_to_width = True
            else:
                glue_to_height = True

        if scale_factor is not None:
            # having the ellipsis glued to height and width doesn't make sense
            assert (not glue_to_width or not glue_to_height)
            assert scale_factor > 0

        # ratio is the ratio width/height
        if split_n == 0:
            # first split is the full bounding box minus the first ellipsis
            first_circle = self._get_circle(
                split_n + 1, canvas_meta, degree_n, scale_factor, rotate_degree, glue_to_width, glue_to_height)
            self.phoc_region = canvas_meta.bounding_box.polygon.buffer(1).difference(
                canvas_meta.bounding_box.polygon.intersection(first_circle))

        elif degree_n > split_n:
            # nth split is the current circle minus the area from the next circle
            outer_circle = self._get_circle(
                split_n, canvas_meta, degree_n, scale_factor, rotate_degree, glue_to_width, glue_to_height)
            inner_circle = self._get_circle(
                split_n + 1, canvas_meta, degree_n, scale_factor, rotate_degree, glue_to_width, glue_to_height)
            self.phoc_region = outer_circle.difference(inner_circle)
        else:
            # last split is just the innermost ellipsis
            self.phoc_region = self._get_circle(
                split_n, canvas_meta, degree_n, scale_factor, rotate_degree, glue_to_width, glue_to_height)
        self.bounds = self.phoc_region.bounds  # (minx, miny, maxx, maxy)

        self.bounding_box = BoundingBox2D(self.bounds[0], self.bounds[1], self.bounds[2], self.bounds[3])
        if self.percentages:
            self.area = self.phoc_region.area
        else:
            self.area = 1
        if not vis and not percentages:
            self.phoc_region = prep(self.phoc_region)

    @classmethod
    def _get_circle(cls, split_n, canvas_meta, degree_n, scale_factor, rotate_degree, glue_to_width, glue_to_height):
        # dividing by split_n shouldn't give /0 error as in split 0 we add 1 to mimic the "next" ellipsis

        if glue_to_height and glue_to_width:
            if canvas_meta.bounding_box.height > canvas_meta.bounding_box.width:
                buffer_dist = canvas_meta.bounding_box.height / 2 - (
                            (split_n - 1) * canvas_meta.bounding_box.height / 2 / degree_n)
                circle = Point(canvas_meta.centroid).buffer(buffer_dist)
                scale_factor = canvas_meta.bounding_box.width / canvas_meta.bounding_box.height
                ell = scale(circle, xfact=scale_factor)
            else:
                buffer_dist = canvas_meta.bounding_box.width / 2 - (
                            (split_n - 1) * canvas_meta.bounding_box.width / 2 / degree_n)
                circle = Point(canvas_meta.centroid).buffer(buffer_dist)
                scale_factor = canvas_meta.bounding_box.height / canvas_meta.bounding_box.width
                ell = scale(circle, yfact=scale_factor)
        elif glue_to_width:
            buffer_dist = canvas_meta.bounding_box.width / 2 - (
                        (split_n - 1) * canvas_meta.bounding_box.width / 2 / degree_n)
            circle = Point(canvas_meta.centroid).buffer(buffer_dist)
            ell = scale(circle, yfact=scale_factor)
        else:
            buffer_dist = canvas_meta.bounding_box.height / 2 - (
                        (split_n - 1) * canvas_meta.bounding_box.height / 2 / degree_n)
            circle = Point(canvas_meta.centroid).buffer(buffer_dist)
            ell = scale(circle, xfact=scale_factor)

        ell_rot = rotate(ell, rotate_degree)

        return ell_rot

    def test(self, candidate: Geo2DCandidate) -> int:
        if self.percentages:
            return candidate.bounding_box.polygon.intersection(self.phoc_region).area / \
                candidate.bounding_box.polygon.area
        if self.phoc_region.contains(candidate.candidate_region):
            return 1
        if self.phoc_region.intersects(candidate.candidate_region):
            return 1

        return 0

    def visualize(self, multi=1):
        path_soup = bs4.BeautifulSoup(
            self.phoc_region.svg(
                fill_color=get_color(self.split_n)),
            'lxml').find('path')
        path_soup['stroke-width'] = f'{20*multi}'
        path_soup['fill-opacity'] = '.4'
        path_soup['stroke-opacity'] = '.7'
        path_soup['stroke'] = "#000000"
        return path_soup.prettify()
