from typing import List

from canvas import Candidate


class PhocSplit:

    def __init__(self):
        self.bounds = None

    def test(self, candidate: Candidate) -> int:
        """tests whether the specific candidate is part of the split, returns 1 for true, 0 for false.
        NOTE: if percentages=True on the phoc class; then instead of 1, it will return the percentage of the symbol that is in the split"""
        pass
    
    def visualize(self, multi=1) -> str:
        """draws the specific split on a svg. returns the svg xml as a string"""
        pass

    def get_intersection_intervals(self, intervals):
        start, end = intervals.pop()
        while intervals:
            start_temp, end_temp = intervals.pop()
            start = max(start, start_temp)
            end = min(end, end_temp)
        return [start, end]

color_list: List[str] = [
        '#FF0000',
        '#FF6400',
        '#FFD900',
        '#D1FF00',
        '#87FF00',
        '#00FF0D',
        '#00FFAB',
        '#00D8FF',
        '#0057FF',
        '#7800FF',
        '#D600FF'
    ]

def get_color(split_n):
    return color_list[split_n % len(color_list)]