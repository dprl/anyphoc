from canvas import Geo2DCandidate
from shapely.prepared import prep
from canvas.boundingbox2d import BoundingBox2D
from canvas.canvasmeta import Geo2DCanvasMeta
from phoc.phocsplit import PhocSplit, get_color
from math import radians, tan
from shapely.geometry import Polygon
import bs4


class HorzLinePhocSplit(PhocSplit):

    def __init__(self, angle, degree_n, split_n, canvas_meta: Geo2DCanvasMeta, vis: bool = False, percentages=False):
        assert angle < 90
        assert angle > -90
        self.percentages = percentages
        self.angle = angle
        sign = 2 * int(angle > 0) - 1
        tan_ang = tan(radians(angle))
        width_halved = (canvas_meta.bounding_box.width / 2)
        tot_splits = degree_n + 1
        self.split_n = split_n
        y_baseline = split_n * (canvas_meta.bounding_box.height / tot_splits) + canvas_meta.bounding_box.min_y
        self.y0 = y_baseline + tan_ang * width_halved
        self.y1 = y_baseline - tan_ang * width_halved
        self.y2 = self.y0 + canvas_meta.bounding_box.height / tot_splits
        self.y3 = self.y1 + canvas_meta.bounding_box.height / tot_splits
        self.x0 = canvas_meta.bounding_box.min_x
        self.x2 = canvas_meta.bounding_box.min_x
        self.x1 = canvas_meta.bounding_box.max_x
        self.x3 = canvas_meta.bounding_box.max_x

        # account for edge cases
        if split_n == 0:
            self.y0 = min(canvas_meta.bounding_box.min_y, self.y0)
            self.y1 = min(canvas_meta.bounding_box.min_y, self.y1)

        elif split_n == degree_n:
            self.y3 = max(canvas_meta.bounding_box.max_y, self.y3)
            self.y2 = max(canvas_meta.bounding_box.max_y, self.y2)

        self.phoc_region = Polygon([
            (self.x0, self.y0),
            (self.x1, self.y1),
            (self.x3, self.y3),
            (self.x2, self.y2)
        ])

        if self.y0 < canvas_meta.bounding_box.min_y or \
                self.y1 < canvas_meta.bounding_box.min_y or \
                self.y2 > canvas_meta.bounding_box.max_y or \
                self.y3 > canvas_meta.bounding_box.max_y:
            self.phoc_region = self.phoc_region.intersection(canvas_meta.bounding_box.polygon)
        self.bounds = self.phoc_region.bounds  # (minx, miny, maxx, maxy)

        self.bounding_box = BoundingBox2D(self.bounds[0], self.bounds[1], self.bounds[2], self.bounds[3])
        self.area = self.phoc_region.area
        if not vis and not percentages:
            self.phoc_region = prep(self.phoc_region)

    def test(self, candidate: Geo2DCandidate) -> int:

        if self.percentages:
            return candidate.bounding_box.polygon.intersection(self.phoc_region).area/\
                candidate.bounding_box.polygon.area
        if self.phoc_region.contains(candidate.candidate_region):
            return 1
        if self.phoc_region.intersects(candidate.candidate_region):
            return 1

        return 0

    def visualize(self, multi=1):
        path_soup = bs4.BeautifulSoup(
            self.phoc_region.svg(
                fill_color=get_color(self.split_n)),
            'lxml').find('path')
        path_soup['stroke-width'] = f'{20 * multi}'
        path_soup['fill-opacity'] = '.4'
        path_soup['stroke-opacity'] = '.7'
        path_soup['stroke'] = "#000000"
        return path_soup.prettify()
