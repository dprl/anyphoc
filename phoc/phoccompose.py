import os.path
from typing import List, Dict, Hashable, Tuple, Callable
import bs4
from copy import copy
from canvas.canvas import Canvas
from canvas.svg.svgcandidate import SVGCandidate
from phoc.phoc import Phoc


def merge_dicts(old_dict, new_dict, init_dict=[1]) -> Dict[Hashable, List]:
    ret_dict = {}

    old_unified = old_dict["__unifiedkeys__"]
    old_dict.pop("__unifiedkeys__")

    new_unified = new_dict["__unifiedkeys__"]
    new_dict.pop("__unifiedkeys__")

    ret_unified = list(set(old_unified) | set(new_unified))

    if len(old_dict.keys()) == 0:
        for k in new_dict.keys():
            new_vector = new_dict[k]
            updated_vector = copy(init_dict)
            updated_vector.extend(new_vector)
            ret_dict[k] = updated_vector
    else:
        for k in old_dict.keys():
            if k in new_dict.keys():
                old_vector = old_dict[k]
                new_vector = new_dict[k]
                old_vector.extend(new_vector)
                ret_dict[k] = old_vector

    ret_dict["__unifiedkeys__"] = ret_unified
    return ret_dict


class PhocCompose:
    phoc_list: List[Phoc]

    def __init__(self, phoc_list: List[Phoc], skip_first_level=False):
        self.skip_first_level=skip_first_level
        self.phoc_list = phoc_list

    def test(self, canvas: Canvas) -> Dict[Hashable, List]:
        phoc_test_union = {"__unifiedkeys__": []}
        init_dict = [1]
        if self.skip_first_level:
            init_dict = []
        for phoc in self.phoc_list:
            phoc_test_union = merge_dicts(phoc_test_union, phoc.test(canvas), init_dict)
        if len(self.phoc_list) == 0:
            for i in canvas.candidates.intersection((-1000000, -10000000, 1000000, 1000000), objects=True):
                cand: SVGCandidate = i.object
                phoc_test_union[cand.key] = [1]
        phoc_test_union["__unifiedkeys__"] = sorted(phoc_test_union["__unifiedkeys__"])
        return phoc_test_union

    def explain(self) -> Tuple[str, int]:

        bit_sum = 0
        if not self.skip_first_level:
            bit_sum += 1
        for phoc in self.phoc_list:
            _, i = phoc.explain()
            bit_sum += i
        return "", bit_sum

    def visualize(self, canvas: Canvas) -> Dict[str, List[str]]:
        result_svg_list = {}
        for phoc in self.phoc_list:
            result_svg_list[phoc.name] = phoc.visualize(canvas)
        return result_svg_list

    def make_mask(self, first_val: float = 1.0) -> List[float]:
        ret = [first_val]
        for phoc in self.phoc_list:
            ret = ret + phoc.make_mask()
        return ret

    def save_svgs(self, canvas: Canvas, out_dir: str):
        final_svg = self.visualize(canvas)
        os.makedirs(os.path.abspath(out_dir), exist_ok=True)
        for k in final_svg.keys():
            for i, svg in enumerate(final_svg[k]):
                with open(os.path.join(out_dir, f'{k}_{i}.svg'), 'w+') as f:
                    f.write(svg)

    def build_html(self, canvas: Canvas, out_dir: str, out_name: str):
        final_svg = self.visualize(canvas)
        os.makedirs(os.path.abspath(out_dir), exist_ok=True)
        index_html_template = self._get_index_template()
        stylesheet = self._get_stylesheet()
        index_html = bs4.BeautifulSoup(index_html_template, 'lxml')
        phoc_wrapper = index_html.find("div", {"class": "phocwrapper"})
        top_level_soup = bs4.BeautifulSoup(canvas.svg, 'lxml')
        canvas_soup_svg = top_level_soup.find('svg')
        if 'viewbox' in canvas_soup_svg.attrs:
            canvas_soup_svg['viewBox'] = canvas_soup_svg['viewbox']
        canvas_soup_svg['width'] = '600px'
        del canvas_soup_svg['height']
        if not self.skip_first_level:
            phoc_wrapper.insert_before(canvas_soup_svg)
        for k in final_svg.keys():
            phoc_wrapper.append(self._build_phoc(final_svg[k]))

        with open(os.path.join(out_dir, f'{out_name}_index.html'), 'w+') as f:
            f.write(index_html.prettify())

        with open(os.path.join(out_dir, 'phocstyle.css'), 'w+') as f:
            f.write(stylesheet)

    def _build_phoc(self, svg_list: List[str]) -> bs4.BeautifulSoup:
        phoc = "<div width=600 class=phoc></div>"
        temp_phoc: bs4.BeautifulSoup = bs4.BeautifulSoup(phoc, 'lxml').html.body.contents[0]
        for level in svg_list:
            temp_phoc.append(self._build_phoc_level(level))
        return temp_phoc

    @classmethod
    def _build_phoc_level(cls, phoc_level_svg: str) -> bs4.BeautifulSoup:
        level = bs4.BeautifulSoup(phoc_level_svg, 'lxml').html.body.contents[0]
        level['width'] = 600
        del level['height']
        level['class'] = 'phoclevel'
        return level

    @classmethod
    def _get_stylesheet(cls) -> str:
        return """
.phoccontainer {
    display: flex;
    flex-direction: column;
    align-items: center;
}      

.phoc {
    display: flex;
    flex-direction: column;
    margin: 5px;
}

.phoclevel {
    margin-top: 10px
}

.phocwrapper {
    display: flex;
    flex-direction: row;
}

svg {
    width: 600px;
}
        """

    @classmethod
    def _get_index_template(cls) -> str:
        return """
<!doctype html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="phocstyle.css">
    </head>
    <body>
        <div class=phoccontainer>
            <div class=phocwrapper>
            </div>
        </div>
    </body>
</html>
        """
