from typing import Tuple, List, Dict, Hashable, Callable
import bs4
from phoc.phocsplit import PhocSplit


def pull_svg_contents(canvas_svg: str, other_svg: str, bounds: Tuple) -> str:
    canvas_soup = bs4.BeautifulSoup(canvas_svg, 'lxml')
    other_soup = bs4.BeautifulSoup(other_svg, 'lxml')
    canvas_soup_svg = canvas_soup.find('svg')
    if 'viewbox' in canvas_soup_svg.attrs:
        canvas_soup_svg['viewBox'] = canvas_soup_svg['viewbox']
    elif bounds is not None:
        canvas_soup_svg['viewBox'] = f"{bounds[0]} {bounds[1]} {bounds[2]} {bounds[3]} "
    other_soup_svg = other_soup.find('path')
    if other_soup_svg is None:
        other_soup_svg = other_soup.find('g')
    if other_soup_svg is None:
        other_soup_svg = other_soup.find('rect')
    canvas_soup_svg.append(other_soup_svg)
    return canvas_soup_svg.prettify()


class Phoc:
    name: str

    def __init__(self, n,
                 level_scale: Callable[[int], float] = lambda x: x,
                 skipping_step: int = 1,
                 percentages: bool = False,
                 starting_level: int = 1):
        self.level_scale = level_scale
        self.n = n
        self.skipping_step = skipping_step
        self.percentages = percentages
        self.starting_level = starting_level

    def test(self, canvas) -> Dict[Hashable, List]:
        """Tests all candidates in canvas to the nth phoc degree"""
        pass

    def test_candidate(self, candidate, canvas_meta) -> List:
        """Tests a candidate from a canvas for membership in each region of this phoc.
        returns the embedding describing such memberships"""
        pass

    def explain(self) -> Tuple[str, int]:
        """returns a string describing this phoc, and the number of bits it represents"""
        pass

    def visualize(self, canvas):
        """returns a svg object depicting the entire canvas with phoc regions overlayed
        if n is omitted all levels are drawn. if n is set that phoc level is drawn"""
        pass

    def expand_splits(self, canvas_meta) -> List[List[PhocSplit]]:
        """returns a list of splits to the nth degree"""
        pass

    def make_mask(self) -> List[float]:
        # the first level is covered by the phoc compose, so we start at the second level which has two bits so 1 +2 = 3

        mask = []
        for deg in range(self.starting_level, self.n + 1, self.skipping_step):
            bits_per_level = deg + 1

            mask = mask + [self.level_scale(deg)] * bits_per_level

        return mask
