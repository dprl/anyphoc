from shapely.geometry import Polygon
from canvas import Geo2DCandidate
from canvas.boundingbox2d import BoundingBox2D


class ObjectCandidate(Geo2DCandidate):

    def __init__(self, object_type: str, object_number: int, label: str, min_x, min_y, max_x, max_y, region_type="bbox",
                 unified=False):
        self.object_type = object_type
        self.object_number = object_number
        self.key = label
        self.unified = unified
        self.bounding_box = BoundingBox2D(min_x, min_y, max_x, max_y)
        if region_type == "line":
            min_x, min_y, max_x, max_y = self.bounding_box.polygon.centroid.bounds
            min_x -= self.bounding_box.width / 2
            max_x += self.bounding_box.width / 2
            self.candidate_region = Polygon([
                (min_x, min_y),
                (max_x, min_y),
                (max_x, max_y),
                (min_x, max_y)
            ])
        elif region_type == "centroid":
            self.candidate_region = self.bounding_box.polygon.centroid
        else:
            # bbox
            self.candidate_region = self.bounding_box.polygon
