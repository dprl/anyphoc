from rtree import index
from canvas.canvas import Geo2DCanvas
from canvas.canvasmeta import Geo2DCanvasMeta


class RegionCanvas(Geo2DCanvas):

    candidates: index.Index
    canvas_meta: Geo2DCanvasMeta

    def __init__(
            self,
            candidates: index.Index,
            canvas_meta: Geo2DCanvasMeta,
            formula_region_number: int,
            page_number: int,
            file_name: str):
        self.candidates = candidates
        self.canvas_meta = canvas_meta
        self.formula_region = formula_region_number
        self.page_number = page_number
        self.file_name = file_name
