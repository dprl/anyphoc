from typing import List

from rtree import index
from svgelements import Path, Matrix, Rect

from canvas.canvasmeta import Geo2DCanvasMeta
from canvas.svg.svgcandidate import SVGCandidate


class MaxBounds:
    left: float = float("inf")
    bottom: float = float("inf")
    top: float = -float("inf")
    right: float = -float("inf")

    def update(self, min_x, min_y, max_x, max_y):
        self.left = min(self.left, min_x)
        self.right = max(self.right, max_x)
        self.top = max(self.top, max_y)
        self.bottom = min(self.bottom, min_y)

    def to_meta(self):
        return Geo2DCanvasMeta(self.left, self.bottom, self.right, self.top)


def scale_bbox(min_x, min_y, max_x, max_y, scale: float):
    if scale == 1:
        return min_x, min_y, max_x, max_y
    else:
        width = max_x - min_x
        height = max_y - min_y

        offset_x = (width - width * scale) / 2
        offset_y = (height - height * scale) / 2

        min_x = min_x + offset_x
        max_x = max_x - offset_x

        min_y = min_y + offset_y
        max_y = max_y - offset_y

        return min_x, min_y, max_x, max_y


def recursive_parse(
        tag, matrix, idx: index.Index, canvas_bounds: MaxBounds, region_type, scale: float, unify_policy: str):
    if tag.name == 'path':
        cand = parse_path(tag, matrix, idx, canvas_bounds, region_type, scale)
        insert_cand(idx, cand)
    elif tag.name == 'g':
        if 'transform' in tag.attrs:
            matrix = Matrix(tag.attrs['transform']) * matrix

        if 'data-mml-node' in tag.attrs:
            if (tag.attrs['data-mml-node'] == 'mi') or (tag.attrs['data-mml-node'] == 'mn'):
                unified_symbols = parse_mi(tag, matrix, idx, canvas_bounds, region_type, scale, unify_policy)
                for cand in unified_symbols:
                    insert_cand(idx, cand)
            else:
                for child in tag.children:
                    recursive_parse(child, matrix, idx, canvas_bounds, region_type, scale, unify_policy)
        else:
            for child in tag.children:
                recursive_parse(child, matrix, idx, canvas_bounds, region_type, scale, unify_policy)
    elif tag.name == 'rect':
        cand = parse_rect(tag, matrix, idx, canvas_bounds, region_type, scale)
        insert_cand(idx, cand)
    elif tag.name == 'svg':
        parse_svg(tag, matrix, idx, canvas_bounds, region_type, scale, unify_policy)


def parse_path(tag, matrix, idx: index.Index, canvas_bounds: MaxBounds, region_type, scale: float) -> SVGCandidate:
    try:
        label = chr(int(tag.attrs['data-c'], 16))
    except KeyError:
        label = "<UNK>"
    # label = normalize('NFKC', label)
    path = Path(tag.attrs['d'])
    if 'transform' in tag.attrs:
        matrix = Matrix(tag.attrs['transform']) * matrix
    path *= matrix
    if path.bbox() is not None:
        min_x, min_y, max_x, max_y = path.bbox()
        if tag.next is not None and label == '√' and tag.next.name == 'rect':
            width = tag.next.attrs['width']
            if width == '0':
                # HACK to avoid case in which width is zero
                width = '0.0001'
            rect = Rect(x=tag.next.attrs['x'], y=tag.next.attrs['y'], width=width,
                        height=tag.next.attrs['height'])
            rect *= matrix
            min_x2, min_y2, max_x2, max_y2 = rect.bbox()

            max_x += float(max_x2 - min_x2)
        canvas_bounds.update(min_x, min_y, max_x, max_y)
        (min_x, min_y, max_x, max_y) = scale_bbox(min_x, min_y, max_x, max_y, scale)
        return SVGCandidate(label, min_x, min_y, max_x, max_y, region_type)


def parse_rect(tag, matrix, idx: index.Index, canvas_bounds: MaxBounds, region_type, scale: float) -> SVGCandidate:
    if 'transform' in tag.attrs:
        matrix = Matrix(tag.attrs['transform']) * matrix
    if "data-mml-node" in tag.parent.attrs and tag.parent.attrs["data-mml-node"] == "mfrac":
        rect = Rect(x=tag.attrs['x'], y=tag.attrs['y'], width=tag.attrs['width'], height=tag.attrs['height'])
        rect *= matrix
        min_x, min_y, max_x, max_y = rect.bbox()
        canvas_bounds.update(min_x, min_y, max_x, max_y)
        (min_x, min_y, max_x, max_y) = scale_bbox(min_x, min_y, max_x, max_y, scale)
        return SVGCandidate('-', min_x, min_y, max_x, max_y, region_type)


def parse_mi(
        tag,
        matrix,
        idx: index.Index,
        canvas_bounds: MaxBounds,
        region_type,
        scale: float,
        unify_policy) -> List[SVGCandidate]:
    cands = []
    for child in tag.children:
        if child.name == 'path':
            cands.append(parse_path(child, matrix, idx, canvas_bounds, region_type, scale))
    if unify_policy == "normal":
        return cands
    elif len(cands) > 0:
        new_cands = []
        combined_cand = cands[0]
        new_cands.append(combined_cand)
        if unify_policy == "left_associate":
            for cand in cands[1:]:
                combined_cand = combine_cands(combined_cand, cand, region_type)
                new_cands.append(combined_cand)
            new_cands[len(new_cands) - 1].unified = False
        elif unify_policy == "left_associate_with_parts":
            for i, cand in enumerate(cands[1:]):
                combined_cand = combine_cands(combined_cand, cand, region_type)
                new_cands.append(cand)
                new_cands.append(combined_cand)
            new_cands[len(new_cands) - 1].unified = False
        elif unify_policy == "full_with_parts":
            for cand in cands[1:]:
                combined_cand = combine_cands(combined_cand, cand, region_type)
                new_cands.append(cand)
            combined_cand.unified = False
            new_cands.append(combined_cand)
        return new_cands
    else:
        # print(tag)
        # print(tag.children)
        # input()
        return []


def parse_svg(tag,
              matrix,
              idx: index.Index,
              canvas_bounds: MaxBounds,
              region_type,
              scale: float,
              unify_policy):
    # this is broken still. example of latex that breaks this below.
    ''''\eqalignno
    {BD: &\qquad
    x + y = 1 & (1)\cr
    EH: & \qquad
    h
    x + e
    y = eh & (2)\cr
    FG: & \qquad(h - 1)
    x + (e - 1)
    y = eh - 1 & (3)}'''

    for child in tag.children:
        recursive_parse(child, matrix, idx, canvas_bounds, region_type, scale, unify_policy)


def combine_cands(cand1: SVGCandidate, cand2: SVGCandidate, region_type) -> SVGCandidate:
    new_key = cand1.key + cand2.key
    (min_x1, min_y1, max_x1, max_y1) = cand1.candidate_region.bounds
    (min_x2, min_y2, max_x2, max_y2) = cand2.candidate_region.bounds
    min_x = min(min_x1, min_x2)
    min_y = min(min_y1, min_y2)
    max_x = max(max_x1, max_x2)
    max_y = max(max_y1, max_y2)
    return SVGCandidate(new_key, min_x, min_y, max_x, max_y, region_type, unified=True)


def insert_cand(idx: index.Index, cand: SVGCandidate) -> None:
    if cand is not None:
        (min_x, min_y, max_x, max_y) = cand.candidate_region.bounds
        idx.insert(len(idx), (min_x, min_y, max_x, max_y), cand)
