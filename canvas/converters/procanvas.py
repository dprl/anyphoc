import os
from typing import List

import bs4
import requests
from rtree import index

from canvas.canvasmeta import Geo2DCanvasMeta
from canvas.converters.svgcanvas import parse
from canvas.pro.objectcandidate import ObjectCandidate
from canvas.pro.regioncanvas import RegionCanvas


def protable_to_region_canvas_list(pro, region_type='bbox') -> List[List[RegionCanvas]]:
    """
    This takes in a pro table and returns a list of list of RegionCanvas
    the first list holds document level lists so if only one document is in the pro table then

    @param
    """
    documents = pro[0]
    doc_list = []
    for document in documents:
        filename = document[0]
        filename = os.path.split(filename)[1]
        filename = os.path.splitext(filename)[0]
        pages = document[1]
        canvas_list = []
        for p_n, page in enumerate(pages):
            for r_n, region in enumerate(page):
                try:
                    new_canvas = region_to_canvas(region, filename, p_n, r_n, region_type)
                    canvas_list.append(new_canvas)
                except Exception as e:
                    print(f"Something went wrong loading canvas from file {filename}, page: {p_n}, region: {r_n}")
                    print(e)

        doc_list.append(canvas_list)
    return doc_list


def region_to_canvas(region, filename, page_number, region_number, region_type) -> RegionCanvas:
    objects = region[1]
    # region_type = region[2]

    for o_n, obj in enumerate(objects):
        # if obj[5] == 'o':
        #    idx.insert(len(idx), (obj[0], obj[1], obj[2], obj[3]), object_to_candidate(obj, o_n, region_type))
        if obj[5] == 'MML':
            mml = "<math>" + obj[4].replace("\\n", "")[1:-1] + "</math>"
            v = requests.post("http://localhost:8045/render-mathml", json={"mathml": mml})
            c = v.text
            soup = bs4.BeautifulSoup(c, 'lxml')
            idx, canvas_bounds = parse(soup.find('svg'), region_type=region_type, unify_policy="normal", scale=1)
            return RegionCanvas(idx, canvas_bounds.to_meta(), region_number, page_number, filename)


def object_to_candidate(obj, object_number, region_type) -> ObjectCandidate:
    object_type = obj[5]
    object_label = obj[4]
    return ObjectCandidate(
        object_type, object_number, object_label, obj[0], obj[1], obj[2], obj[3], region_type=region_type)