from typing import List, Union
import requests
import bs4
import html
from rtree import index
from svgelements import Path, Matrix, Rect
import sys
import re
from canvas.canvasmeta import Geo2DCanvasMeta
from canvas.converters.svgparse import recursive_parse, MaxBounds
from canvas.svg.svgcandidate import SVGCandidate
from canvas.svg.svgcanvas import SVGCanvas


"""
Code adapted from https://gitlab.com/dprl/xyphoc/-/blob/master/input/render.py
"""


def render(latex_string, url) -> Union[str, None]:
    # latex_string = latex_string.replace('\\', '\\\\')
    # latex_string = re.escape(latex_string)
    latex_string = html.unescape(latex_string)
    latex_string = latex_string.replace(r'\toggle', '')
    latex_string = latex_string.replace(r'\endtoggle', '')
    if "\\label" in latex_string:
        latex_string = re.sub(r"\\label(\{[^}]*})?", "", latex_string)
    if "\\tag" in latex_string:
        latex_string = re.sub(r"\\tag[^{]?({[^}]*})?", "", latex_string)
    try:
        response = requests.post(url, json={'latex': latex_string})
    except requests.exceptions.RequestException as e:
        print(e)
        return None

    soup = bs4.BeautifulSoup(response.text, 'lxml')

    if soup.find('g', {'data-mml-node': 'merror'}) is not None:
        print("Error")
        print(soup.find('g', {'data-mml-node': 'merror'}))
        raise

    return str(soup.find('svg'))


def parse(svg, region_type, scale, unify_policy: str) -> (index.Index, MaxBounds):

    idx = index.Index()
    matrix = Matrix()
    canvas_bounds = MaxBounds()
    for child in svg.children:
        recursive_parse(child, matrix, idx, canvas_bounds, region_type, scale, unify_policy)
    return idx, canvas_bounds


def get_canvas_meta(candidates: index.Index) -> Geo2DCanvasMeta:
    bounds = candidates.bounds
    left = bounds[0]
    bottom = bounds[1]
    right = bounds[2]
    top = bounds[3]
    return Geo2DCanvasMeta(left, bottom, right, top)


def svg_to_svg_canvas(svg: str, region_type="bbox", scale=1, unify_policy="normal") -> Union[SVGCanvas, None]:

    soup = bs4.BeautifulSoup(svg, 'lxml')

    try:
        svg_root = soup.find('svg')
        candidates, canvas_bounds = parse(svg_root, region_type, scale, unify_policy)
        canvas = SVGCanvas(candidates, canvas_bounds.to_meta(), svg)

    except Exception as e:
        print(e)
        raise

    if len(candidates) < 1:
        return None

    return canvas


if __name__ == '__main__':
    svg_data = render(r"A=Q^tBQf(re^{it})", "http://127.0.0.1:8081/render/")
    canvas_ = svg_to_svg_canvas(svg_data, "centroid")

    print(canvas_.canvas_meta.bounding_box.polygon)
