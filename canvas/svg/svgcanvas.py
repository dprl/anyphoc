from typing import List
from rtree import index
from canvas.canvas import Geo2DCanvas
from canvas.canvasmeta import Geo2DCanvasMeta
from canvas.svg.svgcandidate import SVGCandidate


class SVGCanvas(Geo2DCanvas):

    candidates: index.Index
    canvas_meta: Geo2DCanvasMeta
    svg: str

    def __init__(self, candidates: index.Index, canvas_meta: Geo2DCanvasMeta, svg: str):
        self.candidates = candidates
        self.canvas_meta = canvas_meta
        self.svg = svg
