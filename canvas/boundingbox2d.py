from shapely.geometry import Polygon


class BoundingBox2D:
    polygon: Polygon
    min_x: float
    min_y: float
    max_x: float
    max_y: float
    width: float
    height: float

    def __init__(self, min_x, min_y, max_x, max_y):
        self.min_x = min_x
        self.min_y = min_y
        self.max_x = max_x
        self.max_y = max_y
        self.polygon = Polygon([
            (min_x, min_y),
            (max_x, min_y),
            (max_x, max_y),
            (min_x, max_y)
        ])
        self.width = max_x - min_x
        self.height = max_y - min_y

    def overlap(self, other: 'BoundingBox2D') -> bool:
        x_left = max(self.min_x, other.min_x)
        y_top = max(self.min_y, other.min_y)
        x_right = min(self.max_x, other.max_x)
        y_bottom = min(self.max_y, other.max_y)

        if x_right < x_left or y_bottom < y_top:
            return False
        return True
