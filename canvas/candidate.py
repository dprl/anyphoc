from typing import Hashable
from shapely.geometry import Polygon
from canvas.boundingbox2d import BoundingBox2D


class Candidate:
    key: str


class Geo2DCandidate(Candidate):
    bounding_box: BoundingBox2D
    candidate_region: Polygon
