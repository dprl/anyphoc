from typing import List, Tuple, Any
from shapely.geometry import Polygon
from canvas import Candidate, Geo2DCandidate
from canvas.canvasmeta import Geo2DCanvasMeta, CanvasMeta


class Canvas:
    candidates: Any
    canvas_meta: CanvasMeta


class Geo2DCanvas(Canvas):
    candidates: List[Geo2DCandidate]
    canvas_meta: Geo2DCanvasMeta
    candidate_region: Polygon
