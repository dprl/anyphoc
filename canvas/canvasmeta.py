from typing import Tuple
from canvas.boundingbox2d import BoundingBox2D


class CanvasMeta:
    """describes metadata related to the canvas"""


class Geo2DCanvasMeta(CanvasMeta):
    """describes metadata related to a 2D canvas"""
    centroid: Tuple[float, float]
    bounding_box: BoundingBox2D

    def __init__(self, min_x, min_y, max_x, max_y):
        self.bounding_box = BoundingBox2D(min_x, min_y, max_x, max_y)
        self.centroid = ((max_x-min_x)/2+min_x, (max_y-min_y)/2+min_y)
