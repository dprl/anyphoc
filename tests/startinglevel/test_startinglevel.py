import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.vertline.vertlinephoc import VertLinePhoc
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_s2_no_skip():
    v_phoc = VertLinePhoc(0, starting_level=2, n=3)
    compose = PhocCompose([
        v_phoc
    ], skip_first_level=True)
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)
    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 0, 1],
        '𝑐': [1, 0, 0, 1, 0, 0, 0],
        '𝑑': [1, 0, 0, 1, 0, 0, 0],
        '-': [1, 0, 1, 1, 0, 0, 1],  #
        '𝑒': [1, 0, 0, 1, 0, 0, 0],
        '𝑓': [1, 0, 1, 1, 0, 0, 1],  #
        '𝑔': [1, 0, 1, 1, 0, 0, 1],
        '+': [1, 1, 0, 1, 1, 0, 0],
        '2': [0, 1, 0, 0, 1, 0, 0],  #
        '4': [0, 1, 0, 0, 0, 1, 0],  #
        '0': [0, 1, 1, 0, 0, 1, 0],  #
        '9': [0, 0, 1, 0, 0, 0, 1],  #
        '𝑏': [0, 0, 1, 0, 0, 0, 1],
        '__unifiedkeys__': []  #
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/startinglevel/", "hv_dom_s2_no_skip")
    assert d == d_truth
    pass


def test_s2_odd_skip():
    v_phoc = VertLinePhoc(0, starting_level=2, n=5, skipping_step=3)
    compose = PhocCompose([
        v_phoc
    ], skip_first_level=True)
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)
    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 0, 0, 1],
        '𝑐': [1, 0, 0, 1, 0, 0, 0, 0, 0],
        '𝑑': [1, 0, 0, 1, 0, 0, 0, 0, 0],
        '-': [1, 0, 1, 1, 0, 0, 0, 1, 1],  #
        '𝑒': [1, 0, 0, 1, 0, 0, 0, 0, 0],
        '𝑓': [1, 0, 1, 1, 0, 0, 0, 1, 1],  #
        '𝑔': [1, 0, 1, 1, 0, 0, 0, 0, 1],
        '+': [1, 1, 0, 0, 1, 1, 0, 0, 0],
        '2': [0, 1, 0, 0, 0, 1, 0, 0, 0],  #
        '4': [0, 1, 0, 0, 0, 0, 1, 0, 0],  #
        '0': [0, 1, 1, 0, 0, 0, 1, 1, 0],  #
        '9': [0, 0, 1, 0, 0, 0, 0, 1, 0],  #
        '𝑏': [0, 0, 1, 0, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []  #
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/startinglevel/", "hv_dom_s2_3skip")
    assert d == d_truth
    pass


def test_s2_even_skip():
    h_phoc = HorzLinePhoc(0, starting_level=2, n=4, skipping_step=2)
    compose = PhocCompose([
        h_phoc
    ], skip_first_level=True)
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)
    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 0],
        '𝑐': [1, 0, 0, 1, 1, 0, 0, 0],
        '𝑑': [1, 1, 0, 0, 1, 0, 0, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 0],  #
        '𝑒': [0, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [0, 1, 1, 0, 0, 1, 1, 1],  #
        '𝑔': [1, 1, 1, 1, 1, 0, 0, 1],
        '+': [0, 1, 0, 0, 1, 1, 0, 0],
        '2': [1, 1, 0, 0, 1, 0, 0, 0],  #
        '4': [1, 1, 0, 0, 1, 1, 0, 0],  #
        '0': [1, 1, 0, 0, 1, 1, 0, 0],  #
        '9': [1, 1, 0, 0, 1, 1, 0, 0],  #
        '𝑏': [0, 0, 1, 0, 0, 0, 1, 1],
        '__unifiedkeys__': []  #
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/startinglevel/", "hv_dom_s2_2skip")
    assert d == d_truth
    pass


def test_s3_even_skip():
    ell_phoc = EllipsisPhoc(starting_level=3, degree_n=5, skipping_step=2)
    compose = PhocCompose([
        ell_phoc
    ], skip_first_level=True)
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)
    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        '𝑐': [1, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        '𝑑': [0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '-': [1, 1, 1, 0, 1, 1, 1, 0, 0, 0],  #
        '𝑒': [0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑓': [1, 1, 1, 0, 1, 1, 1, 0, 0, 0],  #
        '𝑔': [1, 1, 0, 0, 1, 1, 0, 0, 0, 0],
        '+': [0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
        '2': [0, 0, 1, 1, 0, 0, 0, 1, 1, 0],  #
        '4': [0, 0, 1, 1, 0, 0, 0, 0, 1, 1],  #
        '0': [0, 0, 1, 1, 0, 0, 1, 1, 1, 1],  #
        '9': [0, 1, 1, 0, 0, 0, 1, 1, 0, 0],  #
        '𝑏': [1, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        '__unifiedkeys__': []  #
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/startinglevel/", "hv_dom_s3_2skip")
    assert d == d_truth
    pass


def test_s5_even_skip_exp_mask():
    ell_phoc = EllipsisPhoc(starting_level=3, degree_n=5, skipping_step=2, level_scale=lambda x: x)
    h_phoc = HorzLinePhoc(0, starting_level=2, n=4, skipping_step=2, level_scale=lambda x: x)
    v_phoc = VertLinePhoc(0, starting_level=5, n=8, skipping_step=3, level_scale=lambda x: x)
    compose = PhocCompose([
        ell_phoc,
        h_phoc,
        v_phoc
    ], skip_first_level=True)
    m_truth = [1, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 2, 2, 2, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 8, 8, 8, 8, 8, 8, 8, 8, 8]
    assert m_truth == compose.make_mask(1)
