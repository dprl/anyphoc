import pytest

from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.phoccompose import PhocCompose
from phoc.vertline.vertlinephoc import VertLinePhoc
from indexutils.compressbitvectors import compress_bitvectors

def test_hex_encoding():
    with open('tests/data/test_hex.svg', 'r') as f:
        svg_data = f.read()

    canvas_ = svg_to_svg_canvas(svg_data, region_type="bbox")
    vert_phoc = VertLinePhoc(0, 4)
    ell_phoc = EllipsisPhoc(4)
    horz_phoc = HorzLinePhoc(0, 9)

    compose = PhocCompose([
        vert_phoc,
        ell_phoc,
        horz_phoc
    ], skip_first_level=True)
    tested = compose.test(canvas_)
    truth = {'𝑑': '3DD4FFFFFFBEFBFBF9FCF', '𝑦': '3D56FFFED9C61C383C1E0', '𝑥': '3DDCFFFF6E73C78F87C3F', '-': '3DD6FFFD4441060406010', '+': '29885B1B6C638E0E0F03C', '2': '34CEFFFFFFFFFFFB7FBEE', '=': '126259194461060406018', '𝛼': '122152336C63860E07038', '𝑇': '1221F6636CE38E1E0F078'}
    assert compress_bitvectors(tested, "hex") == truth
