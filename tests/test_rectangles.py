import pytest

from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.rectangle.rectanglephoc import RectanglePhoc
from phoc.phoccompose import PhocCompose
from indexutils.compressbitvectors import compress_bitvectors


def test_percentages_riemann():
    with open('tests/data/riemann.svg', 'r') as f:
        svg_data = f.read()

    canvas_ = svg_to_svg_canvas(svg_data, region_type="bbox")

    rec_phoc = RectanglePhoc(5)

    compose = PhocCompose([
        rec_phoc
    ], skip_first_level=True)

    tested = compose.test(canvas_)
    truth = {
        '𝜁': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        '𝑠': [1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        '=': [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
        '∑': [1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1],
        '𝑛': [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0],
        '1': [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        '∞': [1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        '-': [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0]
    }
    assert compress_bitvectors(tested, index_type="symbol-percentage") == truth


def test_percentages_pi_integral():
    with open('tests/data/pi_integral.svg', 'r') as f:
        svg_data = f.read()

    canvas_ = svg_to_svg_canvas(svg_data, region_type="bbox")

    rec_phoc = RectanglePhoc(5)

    compose = PhocCompose([
        rec_phoc
    ], skip_first_level=True)

    tested = compose.test(canvas_)
    truth = {
        '𝜋': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        '=': [1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0],
        '∫': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '∞': [1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        '−': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        '𝑑': [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0],
        '𝑥': [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0],
        '1': [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
        '+': [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0],
        '2': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    }
    assert compress_bitvectors(tested, index_type="symbol-percentage") == truth
