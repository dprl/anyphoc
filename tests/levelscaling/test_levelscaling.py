import pytest

from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.phoccompose import PhocCompose


def test_level_scaling_none():

    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    assert mask == truth


def test_level_scaling_linear():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True,
        level_scale= lambda x: x
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 2, 2, 2, 3, 3, 3, 3]
    assert mask == truth


def test_level_scaling_exp():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True,
        level_scale=lambda x: x**2
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 4, 4, 4, 9, 9, 9, 9]
    assert mask == truth
