import pytest

from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.phoccompose import PhocCompose


def test_level_scaling_skipping_none_even():
    ellipsis_phoc = EllipsisPhoc(
        6,
        glue_to_width=True,
        glue_to_height=True,
        skipping_step=2
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    assert mask == truth


def test_level_scaling_skipping_none_odd():
    ellipsis_phoc = EllipsisPhoc(
        7,
        glue_to_width=True,
        glue_to_height=True,
        skipping_step=3
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    assert mask == truth


def test_level_scaling_linear_skipping_odd():
    ellipsis_phoc = EllipsisPhoc(
        7,
        glue_to_width=True,
        glue_to_height=True,
        skipping_step=3,
        level_scale=lambda x: x

    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 7, 7, 7]
    assert mask == truth


def test_level_scaling_linear_skipping_even():
    ellipsis_phoc = EllipsisPhoc(
        6,
        glue_to_width=True,
        glue_to_height=True,
        skipping_step=2,
        level_scale=lambda x: x

    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5]
    assert mask == truth


def test_level_scaling_exp_skipping_odd():
    ellipsis_phoc = EllipsisPhoc(
        7,
        glue_to_width=True,
        glue_to_height=True,
        skipping_step=3,
        level_scale=lambda x: x**2

    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 16, 16, 16, 16, 16, 49, 49, 49, 49, 49, 49, 49, 49]
    assert mask == truth


def test_level_scaling_exp_skipping_even():
    ellipsis_phoc = EllipsisPhoc(
        6,
        glue_to_width=True,
        glue_to_height=True,
        skipping_step=2,
        level_scale=lambda x: x**2
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    mask = compose.make_mask()
    truth = [1, 1, 1, 9, 9, 9, 9, 25, 25, 25, 25, 25, 25]
    assert mask == truth
