import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.vertline.vertlinephoc import VertLinePhoc
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_l4_h_dom():
    vert_phoc = VertLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg, region_type="centroid")

    d_truth = {
        '𝑎': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '+': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑐': [1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        '𝑑': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0],
        '𝑔': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        'ℎ': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/centroid/", "h_dom_4")
    assert d == d_truth


def test_l4_v_dom():
    vert_phoc = VertLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg, region_type="centroid")

    d_truth = {
        '𝑎': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑐': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑑': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '-': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑔': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/centroid/", "v_dom_4")
    assert d == d_truth

def test_l4_hv_dom():
    vert_phoc = HorzLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg, region_type="centroid")

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑑': [1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0],
        '𝑔': [1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1],
        '+': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '2': [1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        '4': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '0': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '9': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '𝑏': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/centroid/", "hv_dom_4")
    assert d == d_truth


def test_l3_hv_dom():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg, region_type="centroid")

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝑑': [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        '-': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        '𝑓': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '𝑔': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '+': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '2': [1, 0, 1, 0, 0, 1, 0, 0, 1, 0],
        '4': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '0': [1, 0, 1, 0, 0, 1, 0, 0, 1, 1],
        '9': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '𝑏': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/centroid/", "hv_dom_3_glue_both")
    assert d == d_truth