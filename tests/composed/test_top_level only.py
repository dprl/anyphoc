import pytest

from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_top_level():

    compose = PhocCompose([])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)
    d = compose.test(canvas)

    d_truth = {
        '𝑎': [1],
        '𝑐': [1],
        '𝑑': [1],
        '-': [1],
        '𝑒': [1],
        '𝑓': [1],
        '𝑔': [1],
        '+': [1],
        '2': [1],
        '4': [1],
        '0': [1],
        '9': [1],
        '𝑏': [1],
        '__unifiedkeys__': []
    }

    compose.build_html(canvas, "./tests/composed/", "top_level_only")
    #assert d == d_truth