import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.phoccompose import PhocCompose
from phoc.vertline.vertlinephoc import VertLinePhoc
from tests.horzlinephoc.test_no_angle import get_svg


def test_l2_v_dom():
    vert_phoc = VertLinePhoc(0, 2)
    horz_phoc = HorzLinePhoc(0, 2)
    ellipsis_phoc = EllipsisPhoc(
        2,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellipsis_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0],
        '𝑑': [1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0],
        '-': [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '𝑒': [1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
        '𝑓': [1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0],
        '𝑔': [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '+': [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1],
        '2': [1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1],
        '4': [1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1],
        '0': [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1],
        '9': [1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0],
        '𝑏': [1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0],
        '__unifiedkeys__': []
    }

    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/composed/", "hv_dom_2_all")
    assert d == d_truth
