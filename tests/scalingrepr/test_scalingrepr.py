import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.vertline.vertlinephoc import VertLinePhoc
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_l3_hv_dom():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg, scale=.75)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝑑': [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        '-': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],  #
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        '𝑓': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],  #
        '𝑔': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '+': [1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
        '2': [1, 0, 1, 0, 1, 1, 0, 0, 1, 0],  #
        '4': [1, 0, 1, 0, 0, 1, 0, 0, 1, 1],  #
        '0': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],  #
        '9': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],  #
        '𝑏': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '__unifiedkeys__': []  #
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/scalingrepr/", "hv_dom_3_glue_both_scale_75")
    assert d == d_truth