import pytest

from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_l1_v_dom():
    vert_phoc = EllipsisPhoc(
        1,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1],
        '𝑐': [1, 0, 1],
        '𝑑': [1, 0, 1],
        '-': [1, 1, 1],
        '𝑒': [1, 0, 1],
        '𝑓': [1, 0, 1],
        '𝑔': [1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "v_dom_1_glue_both")
    assert d == d_truth


def test_l2_v_dom():
    vert_phoc = EllipsisPhoc(
        2,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 0],
        '𝑐': [1, 0, 1, 0, 1, 0],
        '𝑑': [1, 0, 1, 0, 0, 1],
        '-': [1, 1, 1, 1, 1, 1],
        '𝑒': [1, 0, 1, 0, 0, 1],
        '𝑓': [1, 0, 1, 0, 1, 1],
        '𝑔': [1, 1, 1, 1, 1, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "v_dom_2_glue_both")
    assert d == d_truth


def test_l3_v_dom():
    vert_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '𝑐': [1, 0, 1, 0, 1, 0, 0, 1, 1, 0],
        '𝑑': [1, 0, 1, 0, 0, 1, 0, 0, 1, 1],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑒': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '𝑓': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '𝑔': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "v_dom_3_glue_both")
    assert d == d_truth


def test_l1_h_dom():
    vert_phoc = EllipsisPhoc(
        1,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1],
        '+': [1, 1, 1],
        '𝑏': [1, 1, 1],
        '𝑐': [1, 0, 1],
        '𝑑': [1, 0, 1],
        '𝑒': [1, 0, 1],
        '𝑓': [1, 1, 1],
        '𝑔': [1, 1, 1],
        'ℎ': [1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "h_dom_1_glue_both")
    assert d == d_truth


def test_l2_h_dom():
    ellipsis_phoc = EllipsisPhoc(
        2,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 0],
        '+': [1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 1, 1, 1, 0],
        '𝑐': [1, 0, 1, 0, 1, 1],
        '𝑑': [1, 0, 1, 0, 1, 1],
        '𝑒': [1, 0, 1, 0, 1, 1],
        '𝑓': [1, 1, 1, 1, 1, 1],
        '𝑔': [1, 1, 1, 1, 1, 0],
        'ℎ': [1, 1, 1, 1, 1, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "h_dom_2_glue_both")
    assert d == d_truth


def test_l3_h_dom():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '+': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 1, 1, 1, 0, 1, 1, 1, 0],
        '𝑐': [1, 0, 1, 0, 1, 1, 0, 1, 1, 0],
        '𝑑': [1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
        '𝑒': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '𝑓': [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '𝑔': [1, 1, 1, 1, 1, 0, 1, 1, 1, 0],
        'ℎ': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "h_dom_3_glue_both")
    assert d == d_truth


def test_l1_hv_dom():
    ellipsis_phoc = EllipsisPhoc(
        1,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1],
        '𝑐': [1, 1, 0],
        '𝑑': [1, 0, 1],
        '-': [1, 1, 1],
        '𝑒': [1, 0, 1],
        '𝑓': [1, 1, 1],
        '𝑔': [1, 1, 1],
        '+': [1, 0, 1],
        '2': [1, 0, 1],
        '4': [1, 0, 1],
        '0': [1, 0, 1],
        '9': [1, 0, 1],
        '𝑏': [1, 1, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "hv_dom_1_glue_both")
    assert d == d_truth


def test_l2_hv_dom():
    ellipsis_phoc = EllipsisPhoc(
        2,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1],
        '𝑐': [1, 1, 0, 1, 0, 0],
        '𝑑': [1, 0, 1, 0, 1, 0],
        '-': [1, 1, 1, 1, 1, 0],
        '𝑒': [1, 0, 1, 0, 1, 0],
        '𝑓': [1, 1, 1, 1, 1, 0],
        '𝑔': [1, 1, 1, 1, 1, 0],
        '+': [1, 0, 1, 0, 1, 1],
        '2': [1, 0, 1, 0, 1, 1],
        '4': [1, 0, 1, 0, 0, 1],
        '0': [1, 0, 1, 0, 1, 1],
        '9': [1, 0, 1, 0, 1, 0],
        '𝑏': [1, 1, 0, 1, 0, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "hv_dom_2_glue_both")
    assert d == d_truth


def test_l3_hv_dom():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝑑': [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        '-': [1, 1, 1, 1, 1, 0, 1, 1, 1, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
        '𝑓': [1, 1, 1, 1, 1, 0, 1, 1, 1, 0],
        '𝑔': [1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
        '+': [1, 0, 1, 0, 1, 1, 0, 1, 1, 1],
        '2': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '4': [1, 0, 1, 0, 0, 1, 0, 0, 1, 1],
        '0': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '9': [1, 0, 1, 0, 1, 0, 0, 1, 1, 0],
        '𝑏': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "hv_dom_3_glue_both")
    assert d == d_truth


def test_single():
    ellipsis_phoc = EllipsisPhoc(
        3,
        glue_to_width=True,
        glue_to_height=True
    )
    compose = PhocCompose([
        ellipsis_phoc
    ])
    svg = get_svg('single')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/ellipsisphoc/", "single_3_glue_both")
    assert d == d_truth







