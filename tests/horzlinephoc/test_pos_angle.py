import pytest

from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.horzline.horzlinephoc import HorzLinePhoc
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_l4_v_dom():
    vert_phoc = HorzLinePhoc(3, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0],
        '𝑑': [1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0],
        '𝑔': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/horzlinephoc/", "v_dom_4_ang_3")
    assert d == d_truth


def test_l4_h_dom():
    vert_phoc = HorzLinePhoc(3, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
        '+': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0],
        '𝑐': [1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
        '𝑑': [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0],
        '𝑒': [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1],
        '𝑓': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑔': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1],
        'ℎ': [1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/horzlinephoc/", "h_dom_4_ang_3")
    assert d == d_truth


def test_l4_hv_dom():
    vert_phoc = HorzLinePhoc(3, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑑': [1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0],
        '𝑒': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1],
        '𝑔': [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1],
        '+': [1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
        '2': [1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0],
        '4': [1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
        '0': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
        '9': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0],
        '𝑏': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/horzlinephoc/", "hv_dom_4_ang_3")
    assert d == d_truth


def test_single():
    vert_phoc = HorzLinePhoc(3, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('single')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/horzlinephoc/", "single_4_ang_3")
    assert d == d_truth
