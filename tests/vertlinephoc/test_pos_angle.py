import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.phoccompose import PhocCompose
from phoc.vertline.vertlinephoc import VertLinePhoc
from tests.horzlinephoc.test_no_angle import get_svg


def test_l2_v_dom():
    vert_phoc = VertLinePhoc(5, 2)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 0],
        '𝑐': [1, 1, 1, 0, 1, 0],
        '𝑑': [1, 1, 1, 0, 1, 0],
        '-': [1, 1, 1, 1, 1, 1],
        '𝑒': [1, 1, 1, 0, 1, 0],
        '𝑓': [1, 1, 1, 0, 1, 1],
        '𝑔': [1, 1, 1, 0, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "v_dom_2_ang_5")
    assert d == d_truth


def test_l2_h_dom():
    vert_phoc = VertLinePhoc(5, 2)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0, 1, 0, 0],
        '+': [1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 0, 1, 0, 0],
        '𝑐': [1, 1, 0, 1, 0, 0],
        '𝑑': [1, 1, 0, 0, 1, 0],
        '𝑒': [1, 0, 1, 0, 1, 0],
        '𝑓': [1, 0, 1, 0, 0, 1],
        '𝑔': [1, 0, 1, 0, 0, 1],
        'ℎ': [1, 0, 1, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "h_dom_2_ang_5")
    assert d == d_truth


def test_l2_hv_dom():
    vert_phoc = VertLinePhoc(5, 2)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1],
        '𝑐': [1, 1, 0, 1, 0, 0],
        '𝑑': [1, 1, 0, 1, 0, 0],
        '-': [1, 1, 1, 1, 0, 1],
        '𝑒': [1, 1, 0, 1, 0, 0],
        '𝑓': [1, 1, 1, 1, 0, 1],
        '𝑔': [1, 1, 1, 1, 0, 1],
        '+': [1, 1, 0, 1, 1, 0],
        '2': [1, 1, 0, 0, 1, 0],
        '4': [1, 0, 1, 0, 1, 0],
        '0': [1, 0, 1, 0, 1, 1],
        '9': [1, 0, 1, 0, 0, 1],
        '𝑏': [1, 0, 1, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "hv_dom_2_ang_5")
    assert d == d_truth


def test_single():
    vert_phoc = VertLinePhoc(3, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('single')
    canvas = svg_to_svg_canvas(svg)
    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "single_4_ang_3")
    assert d == d_truth

