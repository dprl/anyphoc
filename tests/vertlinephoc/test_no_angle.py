import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.vertline.vertlinephoc import VertLinePhoc
from phoc.phoccompose import PhocCompose


def get_svg(f_name) -> str:
    with open(f"./tests/data/{f_name}.svg") as f:
        contents = f.read()
    return contents


def test_l1_v_dom():

    vert_phoc = VertLinePhoc(0, 1)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1],
        '𝑐': [1, 1, 1],
        '𝑑': [1, 1, 1],
        '-': [1, 1, 1],
        '𝑒': [1, 1, 1],
        '𝑓': [1, 1, 1],
        '𝑔': [1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    #with open('ok', 'w') as f:
    #    f.write(str(d))
    compose.build_html(canvas, "./tests/vertlinephoc/", "v_dom_1")
    assert d == d_truth


def test_l2_v_dom():
    vert_phoc = VertLinePhoc(0, 2)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 0, 1, 0],
        '𝑐': [1, 1, 1, 0, 1, 0],
        '𝑑': [1, 1, 1, 0, 1, 0],
        '-': [1, 1, 1, 1, 1, 1],
        '𝑒': [1, 1, 1, 0, 1, 0],
        '𝑓': [1, 1, 1, 0, 1, 0],
        '𝑔': [1, 1, 1, 0, 1, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "v_dom_2")
    assert d == d_truth


def test_l3_v_dom():
    vert_phoc = VertLinePhoc(0, 3)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '𝑐': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '𝑑': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑒': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '𝑓': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '𝑔': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "v_dom_3")
    assert d == d_truth


def test_l4_v_dom():
    vert_phoc = VertLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('v_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0],
        '𝑐': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
        '𝑑': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0],
        '-': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑒': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0],
        '𝑔': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "v_dom_4")
    assert d == d_truth


def test_l1_h_dom():
    vert_phoc = VertLinePhoc(0, 1)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0],
        '+': [1, 1, 1],
        '𝑏': [1, 1, 0],
        '𝑐': [1, 1, 0],
        '𝑑': [1, 1, 0],
        '𝑒': [1, 0, 1],
        '𝑓': [1, 0, 1],
        '𝑔': [1, 0, 1],
        'ℎ': [1, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "h_dom_1")
    assert d == d_truth


def test_l2_h_dom():
    vert_phoc = VertLinePhoc(0, 2)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0, 1, 0, 0],
        '+': [1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 0, 1, 0, 0],
        '𝑐': [1, 1, 0, 1, 0, 0],
        '𝑑': [1, 1, 0, 0, 1, 0],
        '𝑒': [1, 0, 1, 0, 1, 0],
        '𝑓': [1, 0, 1, 0, 0, 1],
        '𝑔': [1, 0, 1, 0, 0, 1],
        'ℎ': [1, 0, 1, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "h_dom_2")
    assert d == d_truth


def test_l3_h_dom():
    vert_phoc = VertLinePhoc(0, 3)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '+': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝑐': [1, 1, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑑': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '𝑓': [1, 0, 1, 0, 0, 1, 0, 0, 1, 0],
        '𝑔': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        'ℎ': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "h_dom_3")
    assert d == d_truth


def test_l4_h_dom():
    vert_phoc = VertLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('h_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '+': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '𝑏': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑐': [1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        '𝑑': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        '𝑒': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '𝑓': [1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0],
        '𝑔': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        'ℎ': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "h_dom_4")
    assert d == d_truth


def test_l4_hv_dom():
    vert_phoc = VertLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('hv_dom')
    canvas = svg_to_svg_canvas(svg)

    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1],
        '𝑐': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑑': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '-': [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1],
        '𝑒': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
        '𝑓': [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1],
        '𝑔': [1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1],
        '+': [1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
        '2': [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        '4': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        '0': [1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0],
        '9': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
        '𝑏': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
        '__unifiedkeys__': []
    }


    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "hv_dom_4")
    assert d == d_truth


def test_single():
    vert_phoc = VertLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('single')
    canvas = svg_to_svg_canvas(svg)
    d_truth = {
        '𝑎': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/vertlinephoc/", "single_4")
    assert d == d_truth





