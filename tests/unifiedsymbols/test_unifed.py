import pytest
from canvas.converters.svgcanvas import svg_to_svg_canvas
from phoc.vertline.vertlinephoc import VertLinePhoc
from phoc.phoccompose import PhocCompose
from tests.horzlinephoc.test_no_angle import get_svg


def test_left_associate_without_parts():

    vert_phoc = VertLinePhoc(0, 3)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('cos_pi2')
    canvas = svg_to_svg_canvas(svg, unify_policy="left_associate_with_parts")

    d_truth = {
        'c': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'co': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'cos': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'o': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        's': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '(': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝜋': [1, 1, 0, 1, 0, 0, 1, 1, 0, 0],
        ')': [1, 1, 0, 1, 1, 0, 0, 1, 0, 0],
        '+': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '3': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '3.': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '3.1': [1, 0, 1, 0, 1, 1, 0, 0, 1, 0],
        '3.14': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '3.141': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '3.1415': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '3.14159': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '.': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '1': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '4': [1, 0, 1, 0, 0, 1, 0, 0, 1, 1],
        '5': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '9': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '__unifiedkeys__': sorted(['co', '3.1415', '3.141', '3.14', '3.1', '3.'])
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/unifiedsymbols/", "v_left_assoc_with_parts")
    assert d == d_truth


def test_left_associate_with_parts():
    vert_phoc = VertLinePhoc(0, 3)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('cos_pi2')
    canvas = svg_to_svg_canvas(svg, unify_policy="left_associate")

    d_truth = {
        'c': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'co': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'cos': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '(': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝜋': [1, 1, 0, 1, 0, 0, 1, 1, 0, 0],
        ')': [1, 1, 0, 1, 1, 0, 0, 1, 0, 0],
        '+': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '3': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '3.': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '3.1': [1, 0, 1, 0, 1, 1, 0, 0, 1, 0],
        '3.14': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '3.141': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '3.1415': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '3.14159': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '__unifiedkeys__': sorted(['co', '3.1415', '3.141', '3.14', '3.1', '3.'])
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/unifiedsymbols/", "v_left_assoc_no_parts")
    assert d == d_truth


def test_full_with_parts():
    vert_phoc = VertLinePhoc(0, 3)
    compose = PhocCompose([
        vert_phoc
    ])
    svg = get_svg('cos_pi2')
    canvas = svg_to_svg_canvas(svg, unify_policy="full_with_parts")

    d_truth = {
        'c': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'o': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        's': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        'cos': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '(': [1, 1, 0, 1, 0, 0, 1, 0, 0, 0],
        '𝜋': [1, 1, 0, 1, 0, 0, 1, 1, 0, 0],
        ')': [1, 1, 0, 1, 1, 0, 0, 1, 0, 0],
        '+': [1, 1, 1, 0, 1, 0, 0, 1, 1, 0],
        '3': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '.': [1, 0, 1, 0, 1, 0, 0, 0, 1, 0],
        '1': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '4': [1, 0, 1, 0, 0, 1, 0, 0, 1, 1],
        '5': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '9': [1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
        '3.14159': [1, 0, 1, 0, 1, 1, 0, 0, 1, 1],
        '__unifiedkeys__': []
    }
    d = compose.test(canvas)
    compose.build_html(canvas, "./tests/unifiedsymbols/", "v_full_with_parts")
    assert d == d_truth
